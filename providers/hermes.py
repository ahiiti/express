import requests
import json
from model.status import Status
from model.trackinfo import TrackInfo

# https://www.myhermes.de/services/tracking/shipments?search=<id>
url = 'https://www.myhermes.de/services/tracking/shipments'

def track(id):
    r = requests.get(url, params={'search': id})
    response = json.loads(r.text)
    extracted = response[0]
    current = extracted['lastStatus']
    current = Status(current['dateTime'], current['description'])
    history = extracted['statusHistory']
    history = [Status(entry['dateTime'], entry['description']) for entry in history]
    trackinfo = TrackInfo('hermes', id, current, history)
    return trackinfo
