class Status:
    def __init__(self, time, description):
        self.time = time
        self.description = description
    def print(self):
        print(self.time, end=' ')
        print(self.description)
