class TrackInfo:

    def __init__(self, provider, id, status, history):
        self.provider = provider
        self.id = id
        self.status = status
        self.history = history

    def print(self):
        print('Last status')
        self.status.print()
        print('History')
        [entry.print() for entry in self.history]