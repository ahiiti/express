#!/bin/python3

import argparse
import providers.hermes


def track():
    if args.provider == 'hermes':
        return providers.hermes.track(args.id)
    else:
        print('Provider not supported.')


parser = argparse.ArgumentParser(description='Track parcels.')
parser.set_defaults(func=track)
parser.add_argument('provider', type=str, help='Operator of the shipment')
parser.add_argument('id', type=str, help='Tracking ID')

# parse args
args = parser.parse_args()

# run sub-commands
result = args.func()

result.print()